//
//  DoorOne.swift
//  letsMakeADeal
//
//  Created by Michael Merrill on 10/25/16.
//  Copyright © 2016 Michael Merrill. All rights reserved.
//

import UIKit

class DoorController: UIViewController {

    var prize: Prize?
    
    init(prize: Prize) {
        self.prize = prize
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ImageView
        if let prize = self.prize {
            let imageView = UIImageView(image: prize.image)
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(imageView)
            
            let topContraint = NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0)
            let leadingContraint = NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0)
            let trailingContraint = NSLayoutConstraint(item: imageView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0)
            let aspectRatio = NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: imageView, attribute: .width, multiplier: 1, constant: 0)
            
            NSLayoutConstraint.activate([topContraint, leadingContraint, trailingContraint, aspectRatio])
        }
        
        let closeButton = UIButton(type: .custom)
        closeButton.setTitle("Close", for: .normal)
        closeButton.backgroundColor = .blue
        closeButton.addTarget(self, action: #selector(dismiss(sender:)), for: .touchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(closeButton)
        
        let width = NSLayoutConstraint(item: closeButton, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 0.5, constant: 1)
        let height = NSLayoutConstraint(item: closeButton, attribute: .height, relatedBy: .equal, toItem: closeButton, attribute: .height, multiplier: 1, constant: 44)
        let bottomConstraint = NSLayoutConstraint(item: closeButton, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -16)
        let xConstraint = NSLayoutConstraint(item: closeButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([width, height, bottomConstraint, xConstraint])
    }
    
    func dismiss(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
