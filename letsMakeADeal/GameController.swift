//
//  ViewController.swift
//  letsMakeADeal
//
//  Created by Michael Merrill on 10/25/16.
//  Copyright © 2016 Michael Merrill. All rights reserved.
//

import UIKit

protocol SegueHandlerType {
    associatedtype SegueIdentifier: RawRepresentable
}

extension SegueHandlerType where Self: UIViewController, SegueIdentifier.RawValue == String {
    func performSegue(withIdentifier identifier: SegueIdentifier, sender: Any?) {
        performSegue(withIdentifier: identifier.rawValue, sender: sender)
    }
    
    func segueIdentifier(forSegue segue: UIStoryboardSegue) -> SegueIdentifier {
        guard let identifier = segue.identifier,
            let segueIdentifier = SegueIdentifier(rawValue: identifier) else {
                fatalError("Invalid segue identifier")
        }
        return segueIdentifier
    }
}

class GameController: UIViewController, SegueHandlerType {
    
    enum SegueIdentifier: String {
        case showDoor
    }
    var game = Game()
    var label = UILabel()
    var control = UISegmentedControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Let's Make A Deal"
        self.view.backgroundColor = .white

        resetGame()
        
        // Label
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false

        // Segmented Control
        control.addTarget(self, action: #selector(selectDoor(sender:)), for: .valueChanged)
        
        // StackView
        let stackview = UIStackView(arrangedSubviews: [label, control])
        stackview.axis = .vertical
        stackview.distribution = .fill
        stackview.alignment = .center
        stackview.spacing = 50
        stackview.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(stackview)
        
        // Constraints
        let xContraint = NSLayoutConstraint(item: stackview, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let yContraint = NSLayoutConstraint(item: stackview, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0)
        let widthContraint = NSLayoutConstraint(item: stackview, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 0.75, constant: 1)
        NSLayoutConstraint.activate([xContraint, yContraint, widthContraint])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        resetGame()
        super.viewWillAppear(animated)
    }
    
    func resetGame() {
        game = Game()
        control = UISegmentedControl(items: game.doorNames)
        control.isMomentary = true
        label.text = "Pick a door."
    }
    
    func selectDoor(sender: UISegmentedControl) {
        if game.guesses > 0 {
            performSegue(withIdentifier: .showDoor, sender: sender)
        }
        
        label.text = "There is a goat behind \("door 2"). Want to change your answer?"
        control.isMomentary = false
        game.guesses += 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(forSegue: segue) {
        case .showDoor:
            guard let controller = segue.destination as? DoorController else {
                fatalError("Could not cast destination controller at Door Controller")
            }

            if let control = sender as? UISegmentedControl {
                let door = game.doors[control.selectedSegmentIndex]
                controller.prize = door.prize
            }
        }
    }
}



