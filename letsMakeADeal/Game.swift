//
//  Door.swift
//  letsMakeADeal
//
//  Created by Michael Merrill on 10/29/16.
//  Copyright © 2016 Michael Merrill. All rights reserved.
//

import Foundation
import UIKit

enum Prize {
    case Car
    case Goat
    
    var rawValue: String {
        switch self {
        case .Car: return "Car"
        case .Goat: return "Goat"
        }
    }
}

extension Prize {
    var image: UIImage {
        return UIImage(named: self.rawValue)!
    }
}

struct Door {
    var prize: Prize
}

class Game {
    var guesses = 0
    var doors = [Door]()
    var prizes: [Prize] = [.Goat, .Goat, .Car]
    
    init() {
        prizes.shuffle()
        
        for prize in prizes {
            doors.append(Door(prize: prize))
        }
    }
}

extension Game {
    var doorNames: [String] {
        var names = [String]()
        var index = 1
        for _ in doors {
            names.append("Door \(index)")
            index += 1
        }
        return names
    }
    
//    func index(for prize: Prize) -> Int {
//        
//    }
    
    var goatDoors: [Door] {
        return doors.filter { door in
            door.prize == .Goat
        }
    }
}

extension MutableCollection where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffle() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in startIndex ..< endIndex - 1 {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                swap(&self[i], &self[j])
            }
        }
    }
}
